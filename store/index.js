export const state = () => ({
  miniVariant: false,
})

export const mutations = {
  set_miniVariant(state, newVal) {
    state.miniVariant = newVal
  },
}
